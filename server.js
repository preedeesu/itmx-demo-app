const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const api = require('./api/api');


const app = new express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'dist')));

// Enable CORS
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Heaaders', 'Origin,Content-Type,Content-Length');

    if ('OPTIONS' === req.methods){
        res.header('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
        res.sendStatus(200);
    } else {
        next();
    }
});

app.use('/api', api);
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.PORT || '3000';
app.set('port', port);
const server = http.createServer(app);

server.listen(port, ()=> console.log(`ITMX API is running on port ${port}`));