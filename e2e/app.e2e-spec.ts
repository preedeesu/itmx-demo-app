import { ItmxDemoAppPage } from './app.po';

describe('itmx-demo-app App', () => {
  let page: ItmxDemoAppPage;

  beforeEach(() => {
    page = new ItmxDemoAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
