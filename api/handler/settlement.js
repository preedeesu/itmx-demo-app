const path = require('path');
const Settlement = require('../service/settlement');

const opt = {
    username: process.env.ITMXDEMO_USERNAME,
    mspid: process.env.ITMXDEMO_MSPID,
    cryptoContent: {
        privateKey: process.env.ITMXDEMO_USER_PRIVATE_KEY,
        signedCert: process.env.ITMXDEMO_USER_SIGNED_CERT
    }
};

const channelIds = ['bulk001002', 'bulk001003', 'bulk002003'];
var channels = {};

// Load all channels
channelIds.forEach((channelId) => {
    console.log(`Create channel: ${channelId}`);
    channels[channelId] = new Settlement(channelId, process.env.ITMXDEMO_PEER_ADDRESS, process.env.ITMXDEMO_ORDERER_ADDRESS);
});

exports.settleTransactionHandler = function(req, res){
    let channelId = req.params.channelId;
    let key = req.params.key;

    let settlement = channels[channelId];
    settlement.loadUser(opt)
    .then((user) => {
        return settlement.settleTransaction(key);
    })
    .then((tx_id) => {
        res.statusCode = 200;
        return res.json({tx_id: tx_id});
    })
    .catch((err) => {
        res.statusCode = 500;
        return res.json({error: err});
    });
}

exports.getChannelListHandler = function(req, res){
    res.statusCode = 200;
    return res.json({channels: channelIds});
}

exports.getTransactionsByChannelHandler = function(req, res){
    let channelId = req.params.channelId;
    let start = 'C' + req.query.date;
    let end = 'C' + req.query.date + '999999';

    let settlement = channels[channelId];
    settlement.loadUser(opt)
    .then((user) => {
        return settlement.queryByRange(start, end);
    })
    .then((payload)=> {
        console.log(payload);
        res.statusCode = 200;
        return res.json(payload);
    })
    .catch((err) => {
        console.error(err);
        return res.json({error: err});
    });
}
